import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Router } from '@angular/router';

import { CoreModule } from './core/core.module';
import { HomeModule } from './home/home.module';
import { ItemsModule } from './items/items.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from './shared/shared.module';


import { AngularFireModule } from 'angularfire2';
import { AngularFirestoreModule } from 'angularfire2/firestore';

// import { environment } from '../environments/environment.prod';
import { AppComponent } from './app.component';

import { environment } from '../environments/environment.prod';

import { ItemsService } from './core/services/items/items.service';
import { RouterOutlet } from '@angular/router/src/directives/router_outlet';
import { PageNotFoundModule } from './page-not-found/page-not-found.module';
import { AppRoutingModule } from './app-routing.module';


@NgModule({
  imports: [
    BrowserModule,
    AngularFireModule.initializeApp(environment.firebase),
    AngularFirestoreModule,
    NgbModule.forRoot(),
    RouterModule,
    CoreModule,
    SharedModule,
    AppRoutingModule,
    HomeModule,
 //   ItemsModule,
    PageNotFoundModule,
  ],
  declarations: [
    AppComponent
  ],
  providers: [
    ItemsService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
   constructor(router: Router) {
     if (!environment.production) {
       console.log('Routes: ', JSON.stringify(router.config, undefined, 2));
     }
   }
}
