import { Directive, Input, OnInit, HostBinding } from '@angular/core';
import { Item } from '../../models/item.model';
import { State } from '../../enums/state.enum';

@Directive({
  selector: '[appState]'
})
export class StateDirective implements OnInit {


  @Input('appState') appState: State;
  @HostBinding('class') elementClass: string;

  constructor() {

   }

   ngOnInit(): void {
     console.log(this.appState);
     this.elementClass = this.formatClass(this.appState);
  }

  private removeAccent(state: string): string {
    return state.normalize('NFD').replace(/[\u0300-\u036f]/g, '');
  }

  private formatClass(state: string): string {
    return `state-${(this.removeAccent(state)).toLowerCase().replace(' ', '-')}`;
  }
}
