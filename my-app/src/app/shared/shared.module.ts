import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { NavComponent } from './components/nav/nav.component';
import { RouterModule } from '@angular/router';
import { StateDirective } from './directives/state/state.directive';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    RouterModule,

  ],
  declarations: [NavComponent, StateDirective],
  exports: [
    NavComponent,
    StateDirective
  ],
})
export class SharedModule { }
