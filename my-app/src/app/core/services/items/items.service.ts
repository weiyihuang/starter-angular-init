import { Injectable } from '@angular/core';
import { items } from '../list';
import { Item } from '../../../shared/models/item.model';
import { AngularFirestore, AngularFirestoreCollection } from 'angularfire2/firestore';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ItemsService {

  private itemsCollection: AngularFirestoreCollection<Item>;
  collection$: Observable<Item[]>;

  constructor(private readonly afs: AngularFirestore) {
    this.itemsCollection = afs.collection<Item>('collection');
    this.setCollection(this.itemsCollection.valueChanges() );
  }

  getCollection(): Observable<Item[]> {
    return this.collection$;
  }

  setCollection(collection: Observable<Item[]>) {
    this.collection$ = collection;
  }
}
