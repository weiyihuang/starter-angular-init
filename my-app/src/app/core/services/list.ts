import { Item } from '../../shared/models/item.model';

export const items: Item[] = [
  {
    id: '1',
    name: 'christope',
    reference: '1245',
    state: 'A livrer'
  },
  {
    id: '2',
    name: 'julien',
    reference: '1335',
    state: 'En Cours'
  },
  {
    id: '3',
    name: 'Aurélie',
    reference: '1645',
    state: 'Livrée'
  }
];
